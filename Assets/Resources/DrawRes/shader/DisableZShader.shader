﻿Shader "Hidden/DisableZ"
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_Color("Main Color", Color) = (1,1,1,1)
	}

		SubShader
	{
		Tags{ "RenderType" = "Opaque" }
	

		//	Lighting Off 
		//	Cull Off 
			ZTest Always 
			ZWrite On
		//	Fog{ Mode Off }

		LOD 200

		CGPROGRAM

		float4 _Color;
#pragma surface surf Lambert

		sampler2D _MainTex;

	struct Input
	{
		float2 uv_MainTex;
	};

	void surf(Input IN, inout SurfaceOutput o) {
		half4 c = tex2D(_MainTex, IN.uv_MainTex);
		o.Albedo = c.rgb * _Color.rgb;
		o.Alpha = c.a * _Color.a;
	}
	ENDCG
	}
		FallBack "Diffuse"
}