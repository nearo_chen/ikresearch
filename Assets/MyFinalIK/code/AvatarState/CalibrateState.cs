﻿using UnityEngine;
using System.Collections;
using MyIKSetting;
using System;
using ViveCalibrate;

namespace AvatarState
{
    public class CalibrateState : IAvatarState
    {
        CalibrateBones _calibrateBones;
        GameObject _prefeb;
        AvatarStateMain _avatarStateMain;
        PseudoAvatarState _nextState;

        CalibrateBones.CalibrateData _leftCalibrateData, _rightCalibrateData;

        public void ViveTriggerUp(TotalDefine.ActorSide side)
        {
            OnCalibrate(side);
        }
        public void ViveTriggerDn(TotalDefine.ActorSide side)
        {
        }
        public void ViveTouchpadUp(TotalDefine.ActorSide side)
        {
            OnReset(side);
        }
        public void ViveTouchpadDn(TotalDefine.ActorSide side)
        {
        }


        public void ReleaseState()
        {
            MonoBehaviour.Destroy(_prefeb);
        }

        public void OnCalibrate(TotalDefine.ActorSide side)
        {
            _calibrateBones.TriggerCalibrate(side);

            CalibrateBones.CalibrateData calibrateData = _calibrateBones.CalibrateArm_Update(side);
            if (calibrateData != null)
            {
                //calibrate done
                if (side == TotalDefine.ActorSide.left)
                    _leftCalibrateData = calibrateData;
                else
                    _rightCalibrateData = calibrateData;
            }
        }

        public void OnReset(TotalDefine.ActorSide side)
        {   
            _calibrateBones.CalibrateArm_Reset(side);
        }

        public void ViveTracking(TotalDefine.ActorSide side, bool isTracking)
        {
        }

        public void UpdateState(AvatarStateMain main)
        {
            if (_leftCalibrateData != null)
            {
                PseudoAvatarState.Create(_avatarStateMain, _leftCalibrateData, null);
            }
            else if (_rightCalibrateData != null)
            {
                PseudoAvatarState.Create(_avatarStateMain, null, _rightCalibrateData);
            }
        }

        public static CalibrateState Create(AvatarStateMain main)
        {
            //release first
            main.ReleaseCurrentState();

            //create prefeb and put in child
            GameObject obj = MyHelp.ResourceLoadInChild(main.transform, "MyFinalIK_Prefeb/CalibrateState");

            //set data
            CalibrateBones calibrateBones = obj.GetComponent<CalibrateBones>();
            calibrateBones.SetVive2Bone(main.Vive2Bone);

            //create new state
            CalibrateState newState = new CalibrateState();
            newState._calibrateBones = calibrateBones;
            newState._prefeb = obj;
            newState._avatarStateMain = main;

            main.SetCurrentState(newState);
            return newState;
        }

        public void OnLateUpdate()
        {
            
        }
    }
}