﻿using UnityEngine;
using System.Collections;
using MyIKSetting;
using System;
using ViveCalibrate;
using System.Collections.Generic;

namespace AvatarState
{
    public class PseudoAvatarState : IAvatarState
    {
        bool _createDebug;
        GameObject _statePrefeb;
        string _realAvatarPath;
        Transform _pseudoActorNode;
        Animator _pseudoAnimator, _realAvatarAnimator;
        GameObject _realAvatar;
        int _createDone = 0;

        Transform _debugPelvis, _debugPelvisIsolation;
        bool _viveLTracking, _viveRTracking;
        Transform _IKGoalLeft, _IKGoalRight, _IKGoalHMD;
        
        float _movingThreshold;
        Vector3 _realAvatarPosOffset = new Vector3(0, 0, -0.5f);
        Vector3? _movingTarget;
        Vector3 _movingVectorOld;

        void IAvatarState.ReleaseState()
        {
            MonoBehaviour.Destroy(_statePrefeb);
        }

        void IAvatarState.UpdateState(AvatarStateMain main)
        {
            if (_createDone > 0)
                _createDone++;
            if (_createDone < 3)
                return;
            if (_createDone > 100)
                _createDone = 100;

            if (_createDone == 3)
            {
                _statePrefeb.transform.GetComponent<CreateInOrder>().CreateIK_2ndFrame();
            }
            else if (_createDone == 4)
            {
                _statePrefeb.transform.GetComponent<CreateInOrder>().CreateIK_3rdFrame();
            }
            else if (_createDone == 5)
            {
                _pseudoAnimator.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("Animation/Strafing/Humanoid Strafing and Aiming");

                MyHelpNode.FindTransform(_pseudoActorNode, MyIKHelp.getSideName(TotalDefine.ActorSide.left, TotalDefine.name_IKL_Goal), ref _IKGoalLeft);
                MyHelpNode.FindTransform(_pseudoActorNode, MyIKHelp.getSideName(TotalDefine.ActorSide.right, TotalDefine.name_IKL_Goal), ref _IKGoalRight);
                MyHelpNode.FindTransform(_pseudoActorNode, TotalDefine.name_IKHMDNode, ref _IKGoalHMD);

                _realAvatar = MonoBehaviour.Instantiate(Resources.Load(_realAvatarPath)) as GameObject;
                _realAvatar.transform.parent = _statePrefeb.transform;
                _realAvatar.transform.localPosition = Vector3.zero;
                _realAvatar.transform.localRotation = Quaternion.identity;

                _realAvatarAnimator = _realAvatar.GetComponent<Animator>();
                if (_realAvatarAnimator == null)
                    Debug.LogError("[PseudoAvatarState] : (_realAvatarAnimator == null)");

                //PseudoAvatarStateOnAnimatorIK onik = _realAvatarAnimator.GetComponent<PseudoAvatarStateOnAnimatorIK>();
                //onik.fromAvatar = _animator;
                //onik.toAvatar = _realAvatarAnimator;
            }

            if (_createDone >= 5)
            {
                _realAvatar.transform.rotation = _pseudoActorNode.rotation;
                _realAvatar.transform.position = _pseudoActorNode.position + _pseudoActorNode.TransformVector(_realAvatarPosOffset);
                _realAvatar.transform.localScale = _pseudoActorNode.localScale;


                MyIKHelp.SetPalmTracking(_pseudoActorNode, TotalDefine.ActorSide.left, _viveLTracking);
                MyIKHelp.SetPalmTracking(_pseudoActorNode, TotalDefine.ActorSide.right, _viveRTracking);

                if (_viveLTracking && _IKGoalLeft != null)
                {
                    _IKGoalLeft.position = main.Vive2Bone.GetVivePalmPosition(TotalDefine.ActorSide.left);
                    _IKGoalLeft.rotation = main.Vive2Bone.GetViveController(TotalDefine.ActorSide.left).rotation;
                }

                if (_viveRTracking && _IKGoalRight != null)
                {
                    _IKGoalRight.position = main.Vive2Bone.GetVivePalmPosition(TotalDefine.ActorSide.right);
                    _IKGoalRight.rotation = main.Vive2Bone.GetViveController(TotalDefine.ActorSide.right).rotation;
                }

                if (_IKGoalHMD != null)
                {
                    Vector3 HMDPosition;
                    Quaternion HMDRotation;
                    main.Vive2Bone.GetViveHMDPose(out HMDPosition, out HMDRotation);
                    _IKGoalHMD.position = HMDPosition;
                    _IKGoalHMD.rotation = HMDRotation;
                }

                //Mapping bone
                MyIKHelp.MappingAnimatorA2B_LocalRot(_pseudoAnimator, _realAvatarAnimator);
                Transform fromPelvis = _pseudoAnimator.GetBoneTransform(HumanBodyBones.Hips);
                Transform toPelvis = _realAvatarAnimator.GetBoneTransform(HumanBodyBones.Hips);
                toPelvis.localPosition = fromPelvis.localPosition;
                    //new Vector3(_pseudoActorNode.position.x, 
                    //fromPelvis.position.y,
                    //_pseudoActorNode.position.z);

                //update debug
                if (_debugPelvis != null)
                {
                    Transform avatarPelvis = _pseudoAnimator.GetBoneTransform(HumanBodyBones.Hips);
                    _debugPelvis.position = avatarPelvis.position;
                    _debugPelvis.rotation = avatarPelvis.rotation;
                }
            }

            if (_createDone >= 5)
            {
                //process moving
                AvatarAniController avatarAniController = _pseudoActorNode.GetComponentInChildren<AvatarAniController>();
                avatarAniController.Move(_debugPelvisIsolation, _debugPelvis, _movingThreshold);
                avatarAniController.Trun(_IKGoalHMD.forward);
            }
        }


        void IAvatarState.OnLateUpdate()
        {

        }

        void IAvatarState.ViveTouchpadDn(TotalDefine.ActorSide side)
        {
        }

        void IAvatarState.ViveTouchpadUp(TotalDefine.ActorSide side)
        {
            _main.ReleasePreviousState();
            _main.ReleaseCurrentState();
            CalibrateState.Create(_main);
        }

        void IAvatarState.ViveTracking(TotalDefine.ActorSide side, bool isTracking)
        {
            if (side == TotalDefine.ActorSide.left)
                _viveLTracking = isTracking;
            else
                _viveRTracking = isTracking;

            //Debug.Log("[PseudoAvatarState] : isTracking : " + side.ToString() + " , " + isTracking);
        }

        void IAvatarState.ViveTriggerDn(TotalDefine.ActorSide side)
        {
        }

        void IAvatarState.ViveTriggerUp(TotalDefine.ActorSide side)
        {
        }

        AvatarStateMain _main;

        public static PseudoAvatarState Create(AvatarStateMain main, CalibrateBones.CalibrateData calibrateLeftData, CalibrateBones.CalibrateData calibrateRightData)
        {
            CalibrateBones.CalibrateData calibrateData = (calibrateLeftData != null) ? calibrateLeftData : calibrateRightData;

            //create prefeb and put in child
            GameObject obj = MyHelp.ResourceLoadInChild(main.transform, "MyFinalIK_Prefeb/PseudoAvatarState");
            string avatarPath = obj.GetComponent<CreateAnimator>().AvatarPath;

            //create new state
            PseudoAvatarState newState = new PseudoAvatarState();
            newState._statePrefeb = obj;
            obj.GetComponent<CreateInOrder>().CreateAvatarHMD_1stFrame(avatarPath);

            newState._pseudoActorNode = obj.transform.GetChild(0);
            newState._pseudoActorNode.rotation = Quaternion.LookRotation(main.Vive2Bone.ViveHMD.forward, Vector3.up);
            newState._pseudoActorNode.position = new Vector3(main.Vive2Bone.ViveHMD.position.x, newState._pseudoActorNode.position.y, main.Vive2Bone.ViveHMD.position.z);
            newState._pseudoAnimator = newState._pseudoActorNode.GetComponent<Animator>();

            newState._createDebug = true;
            newState._fitAvatar(calibrateData);
            newState._createDone = 1;
            newState._realAvatarPath = avatarPath;

            newState._movingThreshold = calibrateData.viveHMDMaxHeight * 0.1f;

            //release old state at last
            //main.ReleaseCurrentState();

            newState._main = main;
            main.SetCurrentState(newState, true);

            return newState;
        }

        void _moveFitNeck2UArm(CalibrateBones.CalibrateData calibrateData, Animator animator, TotalDefine.ActorSide side)
        {
            Transform avatarNeck = animator.GetBoneTransform(HumanBodyBones.Neck);
            Transform avatarShoulder = animator.GetBoneTransform((side == TotalDefine.ActorSide.left) ? HumanBodyBones.LeftShoulder : HumanBodyBones.RightShoulder);
            Transform avatarUpperArm = animator.GetBoneTransform((side == TotalDefine.ActorSide.left) ? HumanBodyBones.LeftUpperArm : HumanBodyBones.RightUpperArm);

            List<Transform> Neck2UArmList = new List<Transform>();
            Neck2UArmList.Add(avatarNeck);
            Neck2UArmList.Add(avatarShoulder);
            Neck2UArmList.Add(avatarUpperArm);
            float avatarNeck2UArmLength = MyHelpNode.GetWorldLength(Neck2UArmList);
            float toViveRatio = calibrateData.viveNeck2UArmLength / avatarNeck2UArmLength;

            Vector3 localRight = _statePrefeb.transform.InverseTransformDirection(_pseudoActorNode.right);
            //neck to shoulder
            {

                float avatarNeck2shoulderLength = (avatarNeck.position - avatarShoulder.position).magnitude;
                float viveNeck2ShoulderLength = avatarNeck2shoulderLength * toViveRatio;
                Vector3 shoulderWorld = _statePrefeb.transform.TransformPoint(
                    _statePrefeb.transform.InverseTransformPoint(avatarNeck.position) + (localRight * (float)side * viveNeck2ShoulderLength)
                    );
                avatarShoulder.position = shoulderWorld;

                //neck to shoulder debug
                if (_createDebug)
                {
                    Color debugColor = Color.green;
                    Transform shoulderDebug = MyHelpNode.FindOrNew_InChild(_statePrefeb.transform, "debug_shoulder" + side.ToString());
                    MyHelpNode.AddMesh(shoulderDebug.gameObject, PrimitiveType.Sphere, debugColor);
                    shoulderDebug.localScale = Vector3.one * 0.01f;
                    shoulderDebug.position = shoulderWorld;
                    MyHelpDraw.LineDraw(shoulderDebug, avatarNeck.position, 0.005f, 0.005f, debugColor);
                }
            }

            //upper arm node
            {
                float avatarShoulder2UArmLength = (avatarShoulder.position - avatarUpperArm.position).magnitude;
                float viveShoulder2UArmLength = avatarShoulder2UArmLength * toViveRatio;
                Vector3 upperArmWorld = _statePrefeb.transform.TransformPoint(
                    _statePrefeb.transform.InverseTransformPoint(avatarShoulder.position) + (localRight * (float)side * viveShoulder2UArmLength)
                    );
                avatarUpperArm.position = upperArmWorld;

                //upper arm node debug
                if (_createDebug)
                {
                    Color debugColor = Color.blue;
                    Transform upperArmDebug = MyHelpNode.FindOrNew_InChild(_statePrefeb.transform, "debug_UArm" + side.ToString());
                    MyHelpNode.AddMesh(upperArmDebug.gameObject, PrimitiveType.Sphere, debugColor);
                    upperArmDebug.localScale = Vector3.one * 0.01f;
                    upperArmDebug.position = upperArmWorld;
                    MyHelpDraw.LineDraw(upperArmDebug, avatarShoulder.position, 0.005f, 0.005f, debugColor);
                }

                //fix rotation
                Vector3 XDir = Vector3.right * (float)side;
                Vector3 YDir = -Vector3.forward;
                Vector3 ZDir = Vector3.up;
                Matrix4x4 mat = Matrix4x4.identity;
                mat.SetColumn(0, new Vector4(XDir.x, XDir.y, XDir.z, 1));
                mat.SetColumn(1, new Vector4(YDir.x, YDir.y, YDir.z, 1));
                mat.SetColumn(2, new Vector4(ZDir.x, ZDir.y, ZDir.z, 1));
                avatarUpperArm.localRotation = MyHelp.QuaternionFromMatrix(ref mat) * Quaternion.Euler(90.0f, 0, 0);
            }

            //total length debug
            if (_createDebug)
            {
                Vector3 neck2UArmWorld = _statePrefeb.transform.TransformPoint(
                    _statePrefeb.transform.InverseTransformPoint(avatarNeck.position) + (localRight * (float)side * calibrateData.viveNeck2UArmLength)
                    );
                neck2UArmWorld.y += 0.01f;
                Transform neck2UArmDebug = MyHelpNode.FindOrNew_InChild(_statePrefeb.transform, "debug_neck2UArmTotal" + side.ToString());
                MyHelpNode.AddMesh(neck2UArmDebug.gameObject, PrimitiveType.Sphere, Color.yellow);
                neck2UArmDebug.localScale = Vector3.one * 0.01f;
                neck2UArmDebug.position = neck2UArmWorld;
                Vector3 dest = avatarNeck.position;
                dest.y += 0.01f;
                MyHelpDraw.LineDraw(neck2UArmDebug, dest, 0.005f, 0.005f, Color.yellow);
            }
        }

        void _moveFitUArm2Palm(CalibrateBones.CalibrateData calibrateData, Animator animator, TotalDefine.ActorSide side)
        {
            Transform avatarUpperArm = animator.GetBoneTransform((side == TotalDefine.ActorSide.left) ? HumanBodyBones.LeftUpperArm : HumanBodyBones.RightUpperArm);
            Transform avatarLowerArm = animator.GetBoneTransform((side == TotalDefine.ActorSide.left) ? HumanBodyBones.LeftLowerArm : HumanBodyBones.RightLowerArm);
            Transform avatarHand = animator.GetBoneTransform((side == TotalDefine.ActorSide.left) ? HumanBodyBones.LeftHand : HumanBodyBones.RightHand);
            Transform avatarProximal = animator.GetBoneTransform((side == TotalDefine.ActorSide.left) ? HumanBodyBones.LeftMiddleProximal : HumanBodyBones.RightMiddleProximal);

            List<Transform> UArm2handList = new List<Transform>();
            UArm2handList.Add(avatarUpperArm);
            UArm2handList.Add(avatarLowerArm);
            UArm2handList.Add(avatarHand);
            float avatarUArm2HandLength = MyHelpNode.GetWorldLength(UArm2handList);
            float avatarHand2PalmLength = (avatarProximal.transform.position - avatarHand.transform.position).magnitude * CreatePalmEffector.PalmRatio;
            avatarUArm2HandLength += avatarHand2PalmLength;
            float toViveRatio = calibrateData.viveUArm2PalmLength / avatarUArm2HandLength;
            Vector3 localRight = _statePrefeb.transform.InverseTransformDirection(_pseudoActorNode.right);

            //UArm to LowArm
            {
                float avatarUArm2LArmLength = (avatarUpperArm.position - avatarLowerArm.position).magnitude;
                float viveUArm2LowArmLength = avatarUArm2LArmLength * toViveRatio;
                Vector3 LowArmWorld = _statePrefeb.transform.TransformPoint(
                    _statePrefeb.transform.InverseTransformPoint(avatarUpperArm.position) + (localRight * (float)side * viveUArm2LowArmLength)
                    );
                avatarLowerArm.position = LowArmWorld;

                //neck to shoulder debug
                if (_createDebug)
                {
                    Color debugColor = Color.green;
                    Transform LowArmDebug = MyHelpNode.FindOrNew_InChild(_statePrefeb.transform, "debug_LowArm" + side.ToString());
                    MyHelpNode.AddMesh(LowArmDebug.gameObject, PrimitiveType.Sphere, debugColor);
                    LowArmDebug.localScale = Vector3.one * 0.01f;
                    LowArmDebug.position = LowArmWorld;
                    MyHelpDraw.LineDraw(LowArmDebug, avatarUpperArm.position, 0.005f, 0.005f, debugColor);
                }

                //fix rotation
                Vector3 XDir = Vector3.right * (float)side;
                Vector3 YDir = -Vector3.forward;
                Vector3 ZDir = Vector3.up;
                Matrix4x4 mat = Matrix4x4.identity;
                mat.SetColumn(0, new Vector4(XDir.x, XDir.y, XDir.z, 1));
                mat.SetColumn(1, new Vector4(YDir.x, YDir.y, YDir.z, 1));
                mat.SetColumn(2, new Vector4(ZDir.x, ZDir.y, ZDir.z, 1));
                avatarLowerArm.localRotation = MyHelp.QuaternionFromMatrix(ref mat) * Quaternion.Euler(0.0f, 0, 0);
            }

            //LowArm to Hand
            {
                float avatarLowArm2HandLength = (avatarLowerArm.position - avatarHand.position).magnitude;
                float viveLowArm2HandLength = avatarLowArm2HandLength * toViveRatio;
                Vector3 HandWorld = _statePrefeb.transform.TransformPoint(
                    _statePrefeb.transform.InverseTransformPoint(avatarLowerArm.position) + (localRight * (float)side * viveLowArm2HandLength)
                    );
                avatarHand.position = HandWorld;

                //neck to shoulder debug
                if (_createDebug)
                {
                    Color debugColor = Color.blue;
                    Transform HandDebug = MyHelpNode.FindOrNew_InChild(_statePrefeb.transform, "debug_hand" + side.ToString());
                    MyHelpNode.AddMesh(HandDebug.gameObject, PrimitiveType.Sphere, debugColor);
                    HandDebug.localScale = Vector3.one * 0.01f;
                    HandDebug.position = HandWorld;
                    MyHelpDraw.LineDraw(HandDebug, avatarLowerArm.position, 0.005f, 0.005f, debugColor);
                }

                //fix rotation
                Vector3 XDir = Vector3.right * (float)side;
                Vector3 YDir = -Vector3.forward;
                Vector3 ZDir = Vector3.up;
                Matrix4x4 mat = Matrix4x4.identity;
                mat.SetColumn(0, new Vector4(XDir.x, XDir.y, XDir.z, 1));
                mat.SetColumn(1, new Vector4(YDir.x, YDir.y, YDir.z, 1));
                mat.SetColumn(2, new Vector4(ZDir.x, ZDir.y, ZDir.z, 1));
                avatarHand.localRotation = MyHelp.QuaternionFromMatrix(ref mat) * Quaternion.Euler(0.0f, 0, 0);
            }

            //Hand to Palm(just draw debug)
            {
                float viveHand2PalmLength = avatarHand2PalmLength * toViveRatio;
                Vector3 PalmWorld = _statePrefeb.transform.TransformPoint(
                    _statePrefeb.transform.InverseTransformPoint(avatarHand.position) + (localRight * (float)side * viveHand2PalmLength)
                    );

                //neck to shoulder debug
                if (_createDebug)
                {
                    Color debugColor = Color.red;
                    Transform PalmDebug = MyHelpNode.FindOrNew_InChild(_statePrefeb.transform, "debug_Palm" + side.ToString());
                    MyHelpNode.AddMesh(PalmDebug.gameObject, PrimitiveType.Sphere, debugColor);
                    PalmDebug.localScale = Vector3.one * 0.01f;
                    PalmDebug.position = PalmWorld;
                    MyHelpDraw.LineDraw(PalmDebug, avatarHand.position, 0.005f, 0.005f, debugColor);
                }
            }

            //total length debug
            {
                Vector3 UArmToPalmWorld = _statePrefeb.transform.TransformPoint(
                    _statePrefeb.transform.InverseTransformPoint(avatarUpperArm.position) + (localRight * (float)side * calibrateData.viveUArm2PalmLength)
                    );
                UArmToPalmWorld.y += 0.01f;
                Transform neck2UArmDebug = MyHelpNode.FindOrNew_InChild(_statePrefeb.transform, "debug_UArmToPalmTotal" + side.ToString());
                MyHelpNode.AddMesh(neck2UArmDebug.gameObject, PrimitiveType.Sphere, Color.yellow);
                neck2UArmDebug.localScale = Vector3.one * 0.01f;
                neck2UArmDebug.position = UArmToPalmWorld;
                Vector3 dest = avatarUpperArm.position;
                dest.y += 0.01f;
                MyHelpDraw.LineDraw(neck2UArmDebug, dest, 0.005f, 0.005f, Color.yellow);
            }
        }

        void _scaleFitArm2Palm(CalibrateBones.CalibrateData calibrateData, Animator animator, TotalDefine.ActorSide side)
        {
            Transform avatarUpperArm = animator.GetBoneTransform((side == TotalDefine.ActorSide.left) ? HumanBodyBones.LeftUpperArm : HumanBodyBones.RightUpperArm);
            Transform avatarLowerArm = animator.GetBoneTransform((side == TotalDefine.ActorSide.left) ? HumanBodyBones.LeftLowerArm : HumanBodyBones.RightLowerArm);
            Transform avatarHand = animator.GetBoneTransform((side == TotalDefine.ActorSide.left) ? HumanBodyBones.LeftHand : HumanBodyBones.RightHand);
            Transform avatarProximal = animator.GetBoneTransform((side == TotalDefine.ActorSide.left) ? HumanBodyBones.LeftMiddleProximal : HumanBodyBones.RightMiddleProximal);

            List<Transform> UArm2handList = new List<Transform>();
            UArm2handList.Add(avatarUpperArm);
            UArm2handList.Add(avatarLowerArm);
            UArm2handList.Add(avatarHand);
            float avatarUArm2HandLength = MyHelpNode.GetWorldLength(UArm2handList);
            float avatarHand2PalmLength = (avatarProximal.transform.position - avatarHand.transform.position).magnitude * CreatePalmEffector.PalmRatio;
            float avatarUArm2PalmLength = avatarUArm2HandLength + avatarHand2PalmLength;
            float toViveRatio = calibrateData.viveUArm2PalmLength / avatarUArm2PalmLength;

            avatarUpperArm.localScale = Vector3.one * toViveRatio;

            Vector3 localRight = _statePrefeb.transform.InverseTransformDirection(_pseudoActorNode.right);

            //total length debug
            if (_createDebug)
            {
                Vector3 UArmToPalmWorld = _statePrefeb.transform.TransformPoint(
                    _statePrefeb.transform.InverseTransformPoint(avatarUpperArm.position) + (localRight * (float)side * calibrateData.viveUArm2PalmLength)
                    );
                UArmToPalmWorld.y += 0.01f;
                Transform neck2UArmDebug = MyHelpNode.FindOrNew_InChild(_statePrefeb.transform, "debug_UArmToPalmTotal" + side.ToString());
                MyHelpNode.AddMesh(neck2UArmDebug.gameObject, PrimitiveType.Sphere, Color.yellow);
                neck2UArmDebug.localScale = Vector3.one * 0.01f;
                neck2UArmDebug.position = UArmToPalmWorld;
                Vector3 dest = avatarUpperArm.position;
                dest.y += 0.01f;
                MyHelpDraw.LineDraw(neck2UArmDebug, dest, 0.005f, 0.005f, Color.yellow);
            }
        }

        void _createPevisDebug(Animator animator)
        {
            if (!_createDebug)
                return;

            _debugPelvis = MyHelpNode.FindOrNew_InChild(_statePrefeb.transform, "debug_Pelvis");
            MyHelpNode.AddMesh(_debugPelvis.gameObject, PrimitiveType.Cube, TotalDefine.colorJoint);
            _debugPelvis.localScale = new Vector3(0.005f, 0.05f, 0.05f);

            _debugPelvisIsolation = MyHelpNode.FindOrNew_InChild(_statePrefeb.transform, "debug_Pelvis_Iso");
            _debugPelvisIsolation.position = _pseudoAnimator.GetBoneTransform(HumanBodyBones.Hips).position;
        }

        void _fitAvatar(CalibrateBones.CalibrateData calibrateData)
        {
            Transform headBone = _pseudoAnimator.GetBoneTransform(HumanBodyBones.Head);
            float viveHeadMaxHeight = calibrateData.viveHMDMaxHeight - TotalDefine.HeadToHMD_Up;//0.1 = 10 cm

            //scale root first
            Vector3 localAvatarHead = _statePrefeb.transform.InverseTransformPoint(headBone.position);
            float ratio = viveHeadMaxHeight / localAvatarHead.y;
            _pseudoActorNode.localScale = Vector3.one * ratio;
            Debug.Log("[_fitAvatar] scale: " + localAvatarHead.y + " / " + viveHeadMaxHeight + "=" + ratio);

            //modify shoulder position
            _moveFitNeck2UArm(calibrateData, _pseudoAnimator, TotalDefine.ActorSide.left);
            _scaleFitArm2Palm(calibrateData, _pseudoAnimator, TotalDefine.ActorSide.left);
            //_moveFitUArm2Palm(calibrateData, _animator, TotalDefine.ActorSide.left);

            _moveFitNeck2UArm(calibrateData, _pseudoAnimator, TotalDefine.ActorSide.right);
            _scaleFitArm2Palm(calibrateData, _pseudoAnimator, TotalDefine.ActorSide.right);
            //_moveFitUArm2Palm(calibrateData, _animator, TotalDefine.ActorSide.right);

            _createPevisDebug(_pseudoAnimator);

            Debug.Log("[_fitAvatar] done");
        }


    }
}