﻿using UnityEngine;
using System.Collections;
using MyIKSetting;
using System;

namespace AvatarState
{

    public class InitState : IAvatarState
    {
        public void ViveTriggerUp(TotalDefine.ActorSide side)
        {
        }
        public void ViveTriggerDn(TotalDefine.ActorSide side)
        {
        }
        public void ViveTouchpadUp(TotalDefine.ActorSide side)
        {
        }
        public void ViveTouchpadDn(TotalDefine.ActorSide side)
        {
        }

        public void ReleaseState()
        {
        }

        public void ViveTracking(TotalDefine.ActorSide side, bool isTracking)
        {
        }

        public void UpdateState(AvatarStateMain main)
        {
            CalibrateState.Create(main);

            //ViveCalibrate.CalibrateBones.CalibrateData calibrateData = new ViveCalibrate.CalibrateBones.CalibrateData();
            //calibrateData.viveHMDMaxHeight = 0.1f;
            //calibrateData.viveNeck2ShoulderLength = 0.05f;
            //calibrateData.viveShoulder2PalmLength = 0.05f;
            //PseudoAvatarState.Create(main, calibrateData);
        }

        public void OnLateUpdate()
        {
            
        }
    }
}
