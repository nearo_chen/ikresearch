﻿using UnityEngine;
using System.Collections;
using MyIKSetting;

namespace AvatarState
{
    public interface IAvatarState
    {
        void ViveTriggerUp(TotalDefine.ActorSide side);
        void ViveTriggerDn(TotalDefine.ActorSide side);
        void ViveTouchpadUp(TotalDefine.ActorSide side);
        void ViveTouchpadDn(TotalDefine.ActorSide side);
        void ViveTracking(TotalDefine.ActorSide side, bool isTracking);

        void UpdateState(AvatarStateMain main);
        void ReleaseState();

        void OnLateUpdate();
    }

    public class AvatarStateMain : MonoBehaviour
    {
        public bool LeftForceTracking = false;
        public bool RightForceTracking = false;

        IAvatarState _currentState;
        IAvatarState _previousState;
        //  bool _previousUpdate = false;
        ViveToBone _viveToBone;

        public SteamVR_TrackedObject LocalTest_headTrackedObj;
        public SteamVR_TrackedObject LocalTest_leftTrackedObj;
        public SteamVR_TrackedObject LocalTest_rightTrackedObj;
        //bool? _leftTracking, _rightTracking;

        void Start()
        {
            _viveToBone = GetComponent<ViveToBone>();
            SetCurrentState(new InitState());
        }

        void Update()
        {
            _currentState.UpdateState(this);
            // if (_previousState != null && _previousUpdate)
            //    _previousState.UpdateState(this);

            if (Input.GetKeyUp(KeyCode.A)
              || (LocalTest_rightTrackedObj != null && ViveHelp.IsTriggerPress(LocalTest_rightTrackedObj, ViveHelp.PressState.Up))
              )
            {
                _currentState.ViveTriggerUp(TotalDefine.ActorSide.right);
            }
            else if (Input.GetKeyUp(KeyCode.Z)
                || (LocalTest_rightTrackedObj != null && ViveHelp.IsTouchpadPress(LocalTest_rightTrackedObj, ViveHelp.PressState.Up))
                )
            {
                _currentState.ViveTouchpadUp(TotalDefine.ActorSide.right);
            }

            if (Input.GetKeyUp(KeyCode.S)
                || (LocalTest_leftTrackedObj != null && ViveHelp.IsTriggerPress(LocalTest_leftTrackedObj, ViveHelp.PressState.Up))
                )
            {
                _currentState.ViveTriggerUp(TotalDefine.ActorSide.left);
            }
            else if (Input.GetKeyUp(KeyCode.X)
                || (LocalTest_leftTrackedObj != null && ViveHelp.IsTouchpadPress(LocalTest_leftTrackedObj, ViveHelp.PressState.Up))
                )
            {
                _currentState.ViveTouchpadUp(TotalDefine.ActorSide.left);
            }

            bool isTracking = ViveHelp.IsTracking(LocalTest_leftTrackedObj, LeftForceTracking);
            //if (_leftTracking == null || _leftTracking.Value != isTracking)
            {
                _currentState.ViveTracking(TotalDefine.ActorSide.left, isTracking);
                //_leftTracking = isTracking;
            }

            isTracking = ViveHelp.IsTracking(LocalTest_rightTrackedObj, RightForceTracking);
            //if (_rightTracking == null || _rightTracking.Value != isTracking)
            {
                _currentState.ViveTracking(TotalDefine.ActorSide.right, isTracking);
                //_rightTracking = isTracking;
            }
        }

        public void LateUpdate()
        {
            _currentState.OnLateUpdate();
        }

        public void SetCurrentState(IAvatarState newState)
        {
            SetCurrentState(newState, false);
        }

        /// <summary>
        /// return _previousState
        /// </summary>
        public IAvatarState SetCurrentState(IAvatarState newState, bool preserveOldState)
        {
            _previousState = _currentState;
            // _previousUpdate = false;

            if (_previousState != null && !preserveOldState)
                ReleasePreviousState();

            _currentState = newState;
            Debug.Log("[SetNewState]" + _currentState.GetType());

            return _previousState;
        }

        //public void SetPreviousStillUpdate(bool stillUpdate)
        //{
        //    _previousUpdate = stillUpdate;
        //}

        public void ReleaseCurrentState()
        {
            Debug.Log("[ReleaseCurrentState]" + _currentState.GetType());
            _currentState.ReleaseState();
        }

        public void ReleasePreviousState()
        {
            Debug.Log("[ReleasePreviousState]" + _previousState.GetType());
            _previousState.ReleaseState();
            _previousState = null;
        }

        public ViveToBone Vive2Bone
        {
            get
            {
                return _viveToBone;
            }
        }
    }
}