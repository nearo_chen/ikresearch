﻿using UnityEngine;
using System.Collections;

namespace AvatarState
{
    public class PseudoAvatarStateOnAnimatorIK : MonoBehaviour
    {
        public Animator fromAvatar, toAvatar;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnAnimatorIK()
        {
            if (fromAvatar == null)
                return;
            DoingAnimatorIK();
        }

        public void DoingAnimatorIK()
        {
            for (int a = (int)HumanBodyBones.Hips; a < (int)HumanBodyBones.LastBone; a++)
            {
                Transform fromA = fromAvatar.GetBoneTransform((HumanBodyBones)a);
                if (fromA == null)
                    continue;
                toAvatar.SetBoneLocalRotation((HumanBodyBones)a, fromA.localRotation);
                
            }
            Debug.Log("IAvatarState.DoingAnimatorIK");
        }
    }
}