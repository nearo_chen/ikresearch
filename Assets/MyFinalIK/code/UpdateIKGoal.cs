﻿using UnityEngine;
using System.Collections;
using MyIKSetting;

public class UpdateIKGoal : MonoBehaviour
{
    public Transform ActorNode;
    //bool bInit = false;
    Transform mGoal_R_Elbow, mGoal_L_Elbow, mGoal_R_Hand, mGoal_L_Hand, mIKEnd_LeftHand, mIKEnd_RightHand;
    Vector3 mRElbowVec, mLElbowVec;

    // Use this for initialization
    void Start()
    {
        MyHelpNode.FindTransform(ActorNode, MyIKHelp.getSideName(TotalDefine.ActorSide.right, TotalDefine.name_IKL_Elbow), ref mGoal_R_Elbow);
        MyHelpNode.FindTransform(ActorNode, MyIKHelp.getSideName(TotalDefine.ActorSide.left, TotalDefine.name_IKL_Elbow), ref mGoal_L_Elbow);
        MyHelpNode.FindTransform(ActorNode, MyIKHelp.getSideName(TotalDefine.ActorSide.right, TotalDefine.name_IKL_Goal), ref mGoal_R_Hand);
        MyHelpNode.FindTransform(ActorNode, MyIKHelp.getSideName(TotalDefine.ActorSide.left, TotalDefine.name_IKL_Goal), ref mGoal_L_Hand);
        mRElbowVec = ActorNode.InverseTransformPoint(mGoal_R_Elbow.position) - ActorNode.InverseTransformPoint(mGoal_R_Hand.position);
        mLElbowVec = ActorNode.InverseTransformPoint(mGoal_L_Elbow.position) - ActorNode.InverseTransformPoint(mGoal_L_Hand.position);
    }

    // Update is called once per frame
    void Update()
    {
        mGoal_R_Elbow.localPosition = mGoal_R_Hand.localPosition + mRElbowVec;
        mGoal_L_Elbow.localPosition = mGoal_L_Hand.localPosition + mLElbowVec;
    }
}
