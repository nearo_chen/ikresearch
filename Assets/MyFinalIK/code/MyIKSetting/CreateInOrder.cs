﻿using UnityEngine;
using System.Collections;

namespace MyIKSetting
{
    public class CreateInOrder : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// createHMDPosition first
        /// </summary>
        public void CreateAvatarHMD_1stFrame(string pseudoAvatarPath)
        {
            this.GetComponent<CreateAnimator>().AvatarPath = pseudoAvatarPath;
            this.GetComponent<CreateAnimator>().CreateInOrder();
        }

        /// <summary>
        /// create IK next step(Create FBBIK & hand effectors)
        /// </summary>
        public void CreateIK_2ndFrame()
        {
            this.GetComponent<CreatePalmEffector>().AnimatorNode = this.GetComponent<CreateAnimator>().SeudoAvatar.GetComponent<Animator>();
            this.GetComponent<CreatePalmEffector>().createInOrder();
        }

        /// <summary>
        /// Create HeadEffector
        /// </summary>
        public void CreateIK_3rdFrame()
        {
            this.GetComponent<CreateHeadEffector>().AnimatorNode = this.GetComponent<CreateAnimator>().SeudoAvatar.GetComponent<Animator>();
            //this.GetComponent<CreateHeadEffector>().createAvatarHMD();
            //this.GetComponent<CreateHeadEffector>().createEffector();
            this.GetComponent<CreateHeadEffector>().createInOrder();
        }

        [ContextMenu("Create in order")]
        void createInOrder()
        {
            //this.GetComponent<CreateAnimator>().AvatarPath = "AvatarModel/Dummy/Dummy";
            this.GetComponent<CreateAnimator>().CreateInOrder();

            this.GetComponent<CreatePalmEffector>().AnimatorNode = this.GetComponent<CreateAnimator>().SeudoAvatar.GetComponent<Animator>();
            this.GetComponent<CreatePalmEffector>().createInOrder();

            this.GetComponent<CreateHeadEffector>().AnimatorNode = this.GetComponent<CreateAnimator>().SeudoAvatar.GetComponent<Animator>();
            this.GetComponent<CreateHeadEffector>().createInOrder();
        }
    }
}