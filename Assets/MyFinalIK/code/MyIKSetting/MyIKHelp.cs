﻿using UnityEngine;
using System.Collections;
using RootMotion.FinalIK;

namespace MyIKSetting
{

    public class MyIKHelp
    {

        //public static string getHand_GoalName(TotalDefine.ActorSide side)
        //{
        //    return
        //       (side == TotalDefine.ActorSide.left) ? TotalDefine.name_IKLeftGoal : TotalDefine.name_IKRightGoal;
        //}

        public static string getSideName(TotalDefine.ActorSide side, string leftName)
        {
            return
               (side == TotalDefine.ActorSide.left) ? leftName : leftName.Replace("Left", "Right");
        }

        public static IKEffector getHand_IKEffector(TotalDefine.ActorSide side, FullBodyBipedIK fbbik)
        {
            return
                (side == TotalDefine.ActorSide.left) ? fbbik.solver.leftHandEffector : fbbik.solver.rightHandEffector;
        }

        public static FBIKChain getHandElbowEffector(TotalDefine.ActorSide side, FullBodyBipedIK fbbik)
        {
            return
                (side == TotalDefine.ActorSide.left) ? fbbik.solver.leftArmChain : fbbik.solver.rightArmChain;
        }

        public static IKMappingLimb getHandElbowEffectorMapping(TotalDefine.ActorSide side, FullBodyBipedIK fbbik)
        {
            return
                (side == TotalDefine.ActorSide.left) ? fbbik.solver.leftArmMapping : fbbik.solver.rightArmMapping;
        }



        public static void MappingAnimatorRotation_YZExchange_RotY90(Animator actorFrom, Animator actorTo, HumanBodyBones humanBodyBones)
        {
            Transform from = actorFrom.GetBoneTransform(humanBodyBones);
            Transform to = actorTo.GetBoneTransform(humanBodyBones);

            Matrix4x4 matFrom = Matrix4x4.TRS(Vector3.zero, from.localRotation, Vector3.one);
            Matrix4x4 matTo = Matrix4x4.identity;
            matTo.SetColumn(0, matFrom.GetColumn(0) * 1.0f);
            matTo.SetColumn(1, matFrom.GetColumn(2) * 1.0f);
            matTo.SetColumn(2, matFrom.GetColumn(1) * 1.0f);
            Quaternion rot = MyHelp.QuaternionFromMatrix(ref matTo) * Quaternion.Euler(0, 90, 0);
            to.localRotation = rot;
        }

        public static void MappingAnimatorRotationA2B(Animator actorA, Animator actorB)
        {
            for (int a = (int)HumanBodyBones.Hips; a < (int)HumanBodyBones.LastBone; a++)
            {
                Transform fromA = actorA.GetBoneTransform((HumanBodyBones)a);
                Transform toB = actorB.GetBoneTransform((HumanBodyBones)a);
                //toB.localPosition = fromA.localPosition;

                if (fromA == null || toB == null)
                    continue;
                Matrix4x4 matFrom = Matrix4x4.TRS(Vector3.zero, fromA.localRotation, Vector3.one);
                Matrix4x4 matTo = Matrix4x4.identity;

                matTo.SetColumn(0, matFrom.GetColumn(2) * -1.0f);
                matTo.SetColumn(1, matFrom.GetColumn(0) * -1.0f);
                matTo.SetColumn(2, matFrom.GetColumn(1) * 1.0f);
                toB.localRotation = MyHelp.QuaternionFromMatrix(ref matTo);
            }
        }

        public static void MappingAnimatorA2B_LocalRot(Animator actorA, Animator actorB)
        {
            for (int a = (int)HumanBodyBones.Hips; a < (int)HumanBodyBones.LastBone; a++)
            {
                Transform fromA = actorA.GetBoneTransform((HumanBodyBones)a);
                Transform toB = actorB.GetBoneTransform((HumanBodyBones)a);
                //toB.localPosition = fromA.localPosition;

                if (fromA == null || toB == null)
                    continue;
                toB.localRotation = fromA.localRotation;
            }
        }

        public static void MappingAnimatorRotationA2B_Rigidbody(Animator actorA, Animator actorB)
        {
            for (int a = (int)HumanBodyBones.Hips; a < (int)HumanBodyBones.LastBone; a++)
            {
                Transform fromA = actorA.GetBoneTransform((HumanBodyBones)a);
                Transform toB = actorB.GetBoneTransform((HumanBodyBones)a);

                if (fromA == null || toB == null)
                    continue;

                Rigidbody fromRB = fromA.GetComponent<Rigidbody>();
                Rigidbody toRB = toB.GetComponent<Rigidbody>();

                if (fromRB == null ||
                    toRB == null)
                    continue;
                //toRB.rotation = fromRB.rotation;
                toRB.MoveRotation(fromRB.rotation);
                //Debug.Log("MappingAnimatorRotationA2B_Rigidbody : " + toRB.name);
            }
        }

        //public static void MappingAnimatorRotation_angle(Animator actorFrom, Animator actorTo)
        //{
        //    int a;
        //    //for (a = (int)HumanBodyBones.Hips; a < (int)HumanBodyBones.LastBone; a++)
        //    {
        //        a = (int)HumanBodyBones.Hips;
        //        Transform fromA = actorFrom.GetBoneTransform((HumanBodyBones)a);
        //        Transform toB = actorTo.GetBoneTransform((HumanBodyBones)a);

        //        //if (fromA == null || toB == null)
        //        //continue;

        //        Vector3 dir = Vector3.up * -1.0f;
        //        Quaternion rot = Quaternion.FromToRotation(dir, fromA.up * -1.0f);
        //        toB.rotation = rot * ik.references.leftThigh.rotation;
        //    }
        //}


        public static void SetHeadTracking(Transform actor, bool isHeadEnable)
        {
            FBBIKHeadEffector headEffector = actor.GetComponentInChildren<FBBIKHeadEffector>();
            if (headEffector == null)
                return;
            headEffector.enabled = isHeadEnable;
        }

        public static void SetPalmTracking(Transform actor, TotalDefine.ActorSide side, bool isLHandEnable)
        {
            FullBodyBipedIK fbbik = actor.GetComponentInChildren<FullBodyBipedIK>();
            if (fbbik == null)
                return;

            IKEffector handEffector = (side == TotalDefine.ActorSide.left) ? fbbik.solver.leftHandEffector : fbbik.solver.rightHandEffector;
            handEffector.positionWeight = (isLHandEnable) ? 1.0f : 0.0f;
            handEffector.rotationWeight = (isLHandEnable) ? 1.0f : 0.0f;

            FBIKChain handIKChain = (side == TotalDefine.ActorSide.left) ? fbbik.solver.leftArmChain : fbbik.solver.rightArmChain;
            handIKChain.bendConstraint.weight = (isLHandEnable) ? 1.0f : 0.0f;

            IKMappingLimb handIKLimb = (side == TotalDefine.ActorSide.left) ? fbbik.solver.leftArmMapping : fbbik.solver.rightArmMapping;
            handIKLimb.weight = (isLHandEnable) ? 1.0f : 0.0f;
        }

        //public static Transform SetFBBIKProperHead(FullBodyBipedIK fbbik)
        //{
        //    Animator ani = fbbik.transform.GetComponent<Animator>();
        //    ////如果neck 下面是neck1，那抓neck1來當head effector也沒用，因為FBBIK自動認定的是animator裡面的head，除非FBBIK裡面的head也要去改掉
        //    //此外，headEffector也都抓animator的head來用，所以，最好的方式就是改小綠人的設定
        //    Transform headNode;
        //    //Transform neckBone = ani.GetBoneTransform(HumanBodyBones.Neck);
        //    //if (neckBone.childCount == 1)
        //    //    headNode = neckBone.GetChild(0);
        //    //else
        //    headNode = ani.GetBoneTransform(HumanBodyBones.Head);

        //    //fbbik.references.head = headNode;
        //    return headNode;
        //}

    }
}
