﻿using UnityEngine;
using System.Collections;
using RootMotion.FinalIK;
using RootMotion;

namespace MyIKSetting
{
    public class CreatePalmEffector : MonoBehaviour
    {
        public Animator AnimatorNode;

        [ContextMenu("Create in order")]
        public void createInOrder()
        {
            CreateGoal();
            AutoSetParamater();
        }

        [ContextMenu("Create Full Body Biped IK")]
        public void CreateFBBIK()
        {
            _createFBBIK(AnimatorNode.transform);
        }

        static void _createFBBIK(Transform ActorNode)
        {
            FullBodyBipedIK fbbik = MyHelpNode.FindOrAddComponent<FullBodyBipedIK>(ActorNode);
            BipedReferences references = new BipedReferences();
            BipedReferences.AutoDetectReferences(ref references, ActorNode, new BipedReferences.AutoDetectParams(true, false));

            Transform rootNode = IKSolverFullBodyBiped.DetectRootNodeBone(references);
            fbbik.SetReferences(references, rootNode);
        }

        [ContextMenu("Create Hand Goal and Elbow Target")]
        public void CreateGoal()
        {
            _createHandGoal(TotalDefine.ActorSide.left, AnimatorNode);
            _createHandGoal(TotalDefine.ActorSide.right, AnimatorNode);

            _createElbowTarget(TotalDefine.ActorSide.left, AnimatorNode);
            _createElbowTarget(TotalDefine.ActorSide.right, AnimatorNode);

            //Add Component<UpdateIKGoal>
            Transform IKSettingNode = MyHelpNode.FindOrNew_InChild(AnimatorNode.transform, TotalDefine.name_IKSettingNode);
            UpdateIKGoal updateGoal = MyHelpNode.FindOrAddComponent<UpdateIKGoal>(IKSettingNode);
            updateGoal.ActorNode = AnimatorNode.transform;
        }

        static void _createHandGoal(TotalDefine.ActorSide side, Animator animatorNode)
        {
            float headHeight = TotalDefine.HeadHeight(animatorNode);
            Transform pelvis = animatorNode.GetBoneTransform(HumanBodyBones.Hips);
            //Vector3 goalPos = pelvis.position +
            //    animatorNode.transform.TransformDirection((Vector3.right * (float)side + Vector3.forward).normalized) *
            //    headHeight * 0.2f;
            Vector3 goalPos = pelvis.position + animatorNode.transform.TransformDirection((Vector3.right * (float)side) * headHeight * 0.2f);
            goalPos += animatorNode.transform.TransformDirection(Vector3.forward * headHeight * 0.2f);
            goalPos.y += headHeight * 0.2f;

            Transform IKSettingNode = MyHelpNode.FindOrNew_InChild(animatorNode.transform, TotalDefine.name_IKSettingNode);
            string goalName = MyIKHelp.getSideName(side, TotalDefine.name_IKL_Goal);//getHand_GoalName(side);
            Transform goal = MyHelpNode.FindOrNew_InChild(IKSettingNode, goalName);

            MyHelpNode.AddMesh(goal.gameObject, PrimitiveType.Sphere, TotalDefine.colorIKGoal);

            goal.position = goalPos;
            goal.localScale = Vector3.one * headHeight * 0.05f;
        }

        static void _createElbowTarget(TotalDefine.ActorSide side, Animator animatorNode)
        {
            float headHeight = TotalDefine.HeadHeight(animatorNode);
            Transform pelvis = animatorNode.GetBoneTransform(HumanBodyBones.Hips);
            //Vector3 dir = animatorNode.transform.TransformDirection((Vector3.right * (float)side) * 1.0f);
            //dir += animatorNode.transform.TransformDirection(Vector3.forward * 1.0f);
            //Vector3 goalPos = pelvis.position + dir.normalized * headHeight * 0.5f;
            Vector3 goalPos = pelvis.position + animatorNode.transform.TransformDirection((Vector3.right * (float)side) * headHeight * 0.9f);
            goalPos += animatorNode.transform.TransformDirection(-Vector3.forward * headHeight * 0.1f);
            goalPos.y += headHeight * 0.2f;

            Transform IKSettingNode = MyHelpNode.FindOrNew_InChild(animatorNode.transform, TotalDefine.name_IKSettingNode);
            string goalName = MyIKHelp.getSideName(side, TotalDefine.name_IKL_Elbow);//getHand_GoalName(side);
            Transform goal = MyHelpNode.FindOrNew_InChild(IKSettingNode, goalName);

            MyHelpNode.AddMesh(goal.gameObject, PrimitiveType.Sphere, Color.blue);

            goal.position = goalPos;
            goal.localScale = Vector3.one * headHeight * 0.05f;
        }

        static void _setHandGoal(TotalDefine.ActorSide side, Transform actorNode)
        {
            FullBodyBipedIK fbbik = actorNode.GetComponent<FullBodyBipedIK>();
            IKEffector handEffector = MyIKHelp.getHand_IKEffector(side, fbbik);
            string goalName = MyIKHelp.getSideName(side, TotalDefine.name_IKL_Goal);//getHand_GoalName(side);

            Transform find = null;
            MyHelpNode.FindTransform(actorNode, goalName, ref find);

            handEffector.target = find;
            handEffector.positionWeight = 1.0f;
            handEffector.rotationWeight = 1.0f;

            //Set elbow
            goalName = MyIKHelp.getSideName(side, TotalDefine.name_IKL_Elbow);
            find = null;
            MyHelpNode.FindTransform(actorNode, goalName, ref find);

            FBIKChain chain = MyIKHelp.getHandElbowEffector(side, fbbik);
            IKMappingLimb mapping = MyIKHelp.getHandElbowEffectorMapping(side, fbbik);
            chain.bendConstraint.bendGoal = find;
            chain.bendConstraint.weight = 1.0f;//must 1 to prevent elbow wrong way
            chain.pull = 0.0f;
            chain.reach = 0.0f;
            mapping.weight = 1.0f;
            mapping.maintainRotationWeight = 0.0f;
        }

        public const float PalmRatio = 0.5f;
        static void _setHandlingPosition(TotalDefine.ActorSide side, Animator animator)
        {
            FullBodyBipedIK fbbik = animator.transform.GetComponent<FullBodyBipedIK>();

            //set object on hand position
            //left-------------------------------
            Transform middleFinger = (side == TotalDefine.ActorSide.left) ?
                animator.GetBoneTransform(HumanBodyBones.LeftMiddleProximal) :
                animator.GetBoneTransform(HumanBodyBones.RightMiddleProximal);
            Transform handTransform = (side == TotalDefine.ActorSide.left) ?
                animator.GetBoneTransform(HumanBodyBones.LeftHand) :
                animator.GetBoneTransform(HumanBodyBones.RightHand);
            string IKEndName = MyIKHelp.getSideName(side, TotalDefine.name_IKL_End);

            GameObject endHand = MyHelpNode.FindOrNew_InChild(handTransform, IKEndName).gameObject;
            
            Vector3 dir = (middleFinger.position - handTransform.position).normalized;
            float length = (middleFinger.position - handTransform.position).magnitude;
            endHand.transform.position = handTransform.position + dir * length * PalmRatio;//0.5f;
            endHand.transform.localRotation = Quaternion.identity;

            if (side == TotalDefine.ActorSide.left)
                fbbik.references.leftHand = endHand.transform;
            else
                fbbik.references.rightHand = endHand.transform;
        }

        [ContextMenu("Auto-set Paramaters")]
        void AutoSetParamater()
        {
            FullBodyBipedIK fbbik = AnimatorNode.GetComponent<FullBodyBipedIK>();

            fbbik.solver.iterations = 1;

            _setHandGoal(TotalDefine.ActorSide.left, AnimatorNode.transform);
            _setHandGoal(TotalDefine.ActorSide.right, AnimatorNode.transform);

            //set object on hand position            
            _setHandlingPosition(TotalDefine.ActorSide.left, AnimatorNode);
            _setHandlingPosition(TotalDefine.ActorSide.right, AnimatorNode);

            //this is important to set reference, copy from FBBIK            
            fbbik.solver.rootNode = IKSolverFullBodyBiped.DetectRootNodeBone(fbbik.references);
            fbbik.solver.SetToReferences(fbbik.references, fbbik.solver.rootNode);
        }
    }
}