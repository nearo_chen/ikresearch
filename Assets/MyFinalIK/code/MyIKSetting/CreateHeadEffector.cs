﻿using UnityEngine;
using System.Collections;
using RootMotion.FinalIK;
using System.Collections.Generic;

namespace MyIKSetting
{
    public class CreateHeadEffector : MonoBehaviour
    {
        public Animator AnimatorNode;


        [ContextMenu("Create in order")]
        public void createInOrder()
        {
            createAvatarHMD();
            createEffector();
        }

        [ContextMenu("Create Head Effector")]
        void createEffector()
        {
            _createHeadEffector(AnimatorNode.transform);
        }

        static void _createHeadEffector(Transform actorNode)
        {
            FullBodyBipedIK fbbik = actorNode.gameObject.GetComponent<FullBodyBipedIK>();
            if (fbbik == null)
                Debug.LogError("[_createHeadEffector] fbbik == null");
            Animator animator = actorNode.GetComponent<Animator>();

            Transform IKSettingNode = MyHelpNode.FindOrNew_InChild(actorNode, TotalDefine.name_IKSettingNode);
            Transform headEffectorNode = MyHelpNode.FindOrNew_InChild(IKSettingNode, TotalDefine.name_HeadEffector);
            FBBIKHeadEffector headEffector = MyHelpNode.FindOrAddComponent<FBBIKHeadEffector>(headEffectorNode);

            Transform headBone = animator.GetBoneTransform(HumanBodyBones.Head);
            Transform hmdNode = null;
            MyHelpNode.FindTransform(IKSettingNode, TotalDefine.name_IKHMDNode, ref hmdNode);// MyHelpNode.FindOrNew_InChild(IKSettingNode, );
            headEffectorNode.parent = hmdNode;
            headEffectorNode.position = headBone.position;
            headEffectorNode.rotation = headBone.rotation;

            headEffector.ik = fbbik;

            headEffector.bendWeight = 1.0f;//bend weight 0 will not effect aim IK
            headEffector.rotationWeight = 1.0f;// weight 0 will not effect aim IK
            headEffector.bodyWeight = 1.0f;
            headEffector.thighWeight = 1.0f;

            headEffector.bendBones = new FBBIKHeadEffector.BendBone[4];
            //headEffector.bendBones[4] = new FBBIKHeadEffector.BendBone();
            //headEffector.bendBones[4].transform = animator.GetBoneTransform(HumanBodyBones.Hips);
            //headEffector.bendBones[4].weight = 0.1f;

            headEffector.bendBones[3] = new FBBIKHeadEffector.BendBone();
            headEffector.bendBones[3].transform = animator.GetBoneTransform(HumanBodyBones.Spine);
            headEffector.bendBones[3].weight = 1.0f;

            headEffector.bendBones[2] = new FBBIKHeadEffector.BendBone();
            headEffector.bendBones[2].transform = animator.GetBoneTransform(HumanBodyBones.Chest);
            headEffector.bendBones[2].weight = 1.0f;

            headEffector.bendBones[1] = new FBBIKHeadEffector.BendBone();
            headEffector.bendBones[1].transform = animator.GetBoneTransform(HumanBodyBones.Neck);
            headEffector.bendBones[1].weight = 1.0f;

            headEffector.bendBones[0] = new FBBIKHeadEffector.BendBone();
            headEffector.bendBones[0].transform = animator.GetBoneTransform(HumanBodyBones.Head);
            headEffector.bendBones[0].weight = 1.0f;

            //create mesh for head effector
            Renderer renderer = MyHelpNode.AddMesh(headEffectorNode.gameObject, PrimitiveType.Cube, MyHelpDraw.GetShader_DisableZ());
            MyHelpDraw.SetColor_DisableZ(renderer.sharedMaterial, TotalDefine.colorIKEffector);

            float headHeight = TotalDefine.HeadHeight(animator);
            headEffectorNode.localScale = new Vector3(0.1f, 0.1f, 0.5f) * headHeight * 0.3f;
        }

        [ContextMenu("Create HMD test")]
        void createAvatarHMD()
        {
            _createHMDPosition(AnimatorNode);
        }

        //void _createHMDPosition(Animator actorNode, Vector2 head2HMDOffset)
        //{
        //    Transform IKSettingNode = MyHelpNode.FindOrNew_InChild(actorNode.transform, TotalDefine.name_IKSettingNode);
        //    Transform hmdNode = MyHelpNode.FindOrNew_InChild(IKSettingNode, TotalDefine.name_IKHMDNode);
        //    Transform headNode = actorNode.GetBoneTransform(HumanBodyBones.Head);

        //    //Create Head End(HMD Position on head)
        //    hmdNode.position = headNode.TransformPoint(new Vector3(
        //        head2HMDOffset.x,
        //        head2HMDOffset.y,
        //        0));

        //    //create HMD debug box
        //    Transform debugBox = MyHelpNode.FindOrNew_InChild(actorNode.transform, TotalDefine.name_IKHMDNode + "_Box");
        //    MyHelpNode.AddMesh(debugBox.gameObject, PrimitiveType.Cube, TotalDefine.colorViveDevice);
        //    float headHeight = TotalDefine.HeadHeight(actorNode);
        //    debugBox.parent = hmdNode;
        //    debugBox.localScale = new Vector3(0.3f, 0.07f, 0.07f) * headHeight * 0.3f;
        //    debugBox.localPosition = Vector3.zero;
        //    debugBox.localRotation = Quaternion.identity;
        //}

        void _createHMDPosition(Animator actorNode)
        {
            Transform headBone = actorNode.GetBoneTransform(HumanBodyBones.Head);
            Transform IKSettingNode = MyHelpNode.FindOrNew_InChild(actorNode.transform, TotalDefine.name_IKSettingNode);
            Transform hmdNode = MyHelpNode.FindOrNew_InChild(IKSettingNode, TotalDefine.name_IKHMDNode);

            //Create Head Effector(HMD Position on head)
            Vector3 hmdPosition = actorNode.transform.InverseTransformPoint(headBone.position) + new Vector3(
                0,
                TotalDefine.HeadToHMD_Up,
                TotalDefine.HeadToHMD_Forward);

            hmdNode.position = actorNode.transform.TransformPoint(hmdPosition);

            //create HMD debug box
            Transform debugBox = MyHelpNode.FindOrNew_InChild(actorNode.transform, TotalDefine.name_IKHMDNode + "_Box");
            MyHelpNode.AddMesh(debugBox.gameObject, PrimitiveType.Cube, TotalDefine.colorViveDevice);
            float headHeight = TotalDefine.HeadHeight(actorNode);
            debugBox.parent = hmdNode;
            debugBox.localScale = new Vector3(0.3f, 0.07f, 0.07f) * headHeight * 0.3f;
            debugBox.localPosition = Vector3.zero;
            debugBox.localRotation = Quaternion.identity;
        }
    }
}