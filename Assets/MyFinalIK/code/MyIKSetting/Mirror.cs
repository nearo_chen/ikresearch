﻿using UnityEngine;
using System.Collections;

namespace MyIKSetting
{
    public class Mirror : MonoBehaviour
    {
        public Vector2 TextureSize = Vector2.one * 256;
        public Vector2 PlaneSize = Vector2.one;
        public Transform View;
        public float PlaneDistance;
        public float CameraHeight;
        public float CameraFOV = 60;

        //Material _forwardCameraMat;
        Camera _forwardCamera;

        RenderTexture _forwardCameraTex;
        GameObject _forwardCameraPlane;

        // Use this for initialization
        void Start()
        {
            CreateForwardCamera(new Vector3(2, 2.2f), 2, -0.19f, 60);
        }

        // Update is called once per frame
        void Update()
        {
            if (_forwardCameraPlane != null)
            {
                _forwardCameraPlane.transform.localScale = new Vector3(-PlaneSize.x, PlaneSize.y, 1);//x negative for flipping texture
                
                Vector3 f = View.forward;
                f.y = 0;
                f.Normalize();
                Vector3 p = View.localPosition;
                p.y = 0;
                p = View.parent.transform.TransformPoint(p);
                _forwardCameraPlane.transform.position = p + f * PlaneDistance + Vector3.up * PlaneSize.y * 0.5f;
                _forwardCameraPlane.transform.rotation = Quaternion.Euler(0, View.rotation.eulerAngles.y, 0);

                _forwardCamera.transform.localPosition = Vector3.up * CameraHeight;
                //new Vector3(_forwardCamera.transform.localPosition.x,
                //CameraHeight,
                //_forwardCamera.transform.localPosition.z);

                _forwardCamera.fieldOfView = CameraFOV;
            }
        }

        public void CreateForwardCamera(Vector2 size, float planeDistance, float cameraHeight, float cameraFOV)
        {
            if (_forwardCameraTex != null)
                return;

            PlaneSize = size;
            PlaneDistance = planeDistance;
            CameraHeight = cameraHeight;
            CameraFOV = cameraFOV;

            //create texture
            _forwardCameraTex = new RenderTexture((int)TextureSize.x, (int)TextureSize.y, 24);

            //create material
            //Shader shader = Shader.Find("Standard");
            //_forwardCameraMat = new Material(shader);
            //_forwardCameraMat.SetTexture("_MainTex", _forwardCameraTex);

            //create plane
            _forwardCameraPlane = new GameObject();
            _forwardCameraPlane.name = "MirrorPlane_Forward";

            Shader shader = //Shader.Find("Standard");
                Shader.Find("Unlit/Texture");
            MeshRenderer renderer = MyHelpNode.AddMesh(_forwardCameraPlane, PrimitiveType.Quad, shader);

            //set material
            //_forwardCameraMat = renderer.material;
            renderer.material.SetTexture("_MainTex", _forwardCameraTex);

            //create camera
            GameObject go = new GameObject("MirrorCamera_forward");
            _forwardCamera = go.AddComponent<Camera>();
            _forwardCamera.useOcclusionCulling = false;

            //Set position
            _forwardCamera.transform.parent = _forwardCameraPlane.transform;
            _forwardCamera.transform.localPosition = Vector3.zero;
            _forwardCamera.transform.localRotation = Quaternion.Euler(0, 180, 0);

            _forwardCameraPlane.transform.parent = View.parent;
            //_forwardCameraPlane.transform.position = Actor.position + Actor.forward * Distance;
            //_forwardCameraPlane.transform.rotation = Actor.rotation;

            //set camera render texture
            _forwardCamera.targetTexture = _forwardCameraTex;
            _forwardCamera.renderingPath = RenderingPath.Forward;
            _forwardCamera.backgroundColor = Color.white;

            Debug.Log("[Mirror] _createForwardCamera done");
        }
    }


}
