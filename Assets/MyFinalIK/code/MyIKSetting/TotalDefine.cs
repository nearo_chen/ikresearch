﻿using UnityEngine;
using System.Collections;

namespace MyIKSetting
{
    public class TotalDefine
    {
        public const string name_IKSettingNode = "MyIK_Setting";
        public const string name_IKL_Goal = "MyIK_LeftGoal";
        public const string name_IKL_End = "MyIK_LeftEnd";
        public const string name_IKL_Elbow = "MyIK_LeftGoal_Elbow";
        public const string name_HeadEffector = "MyIK_HeadEffector";
        public const string name_IKHMDNode = "MyIK_HeadHMD";

        public static readonly Color colorViveDevice = Color.red;
        public static readonly Color colorIKGoal = Color.red;
        public static readonly Color colorIKEffector = Color.green;
        public static readonly Color colorJoint = Color.cyan;
        
        public enum ActorSide
        {
            left = -1,
            right = 1,
            head,
        }

        public static float HeadHeight(Animator animatorNode)
        {
            return animatorNode.GetBoneTransform(HumanBodyBones.Head).position.y - animatorNode.transform.position.y;
        }

        public const float HeadToHMD_Up = 0.1f;//0.1 = 10 cm
        public const float HeadToHMD_Forward = 0.1f;//0.1 = 10 cm
    }
}
