﻿using UnityEngine;
using System.Collections;
using RootMotion.FinalIK;
using RootMotion;

namespace MyIKSetting
{
    public class CreateAnimator : MonoBehaviour
    {
        public string AvatarPath;
        public GameObject SeudoAvatar;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        [ContextMenu("Create in order")]
        public void CreateInOrder()
        {
            if (transform.childCount != 0)
            {
                Debug.LogWarning("[CreateAnimator] already has avatar in child, transform.childCount != 0");
                //return;
                SeudoAvatar = transform.GetChild(0).gameObject;
            }
            else
            {
                SeudoAvatar = MyHelp.ResourceLoadInChild(transform, AvatarPath);
            }

            SeudoAvatar.name = "Pseudo_" + SeudoAvatar.name;
            Animator ani = MyHelpNode.FindOrAddComponent<Animator>(SeudoAvatar.transform);
            ani.applyRootMotion = true;
            ani.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("Animation/Strafing/nothing");//("Animation/Strafing/Humanoid Strafing and Aiming");

            //set pseudo avatar transparent
            SkinnedMeshRenderer skinRenderer = SeudoAvatar.GetComponentInChildren<SkinnedMeshRenderer>();
            Material newMaterial = Instantiate<Material>(skinRenderer.sharedMaterial); //new Material(Shader.Find("Legacy Shaders/Transparent/VertexLit"));
            newMaterial.shader = Shader.Find("Legacy Shaders/Transparent/VertexLit");
            newMaterial.SetColor("_Color", new Vector4(1, 1, 1, 0.5f));
            skinRenderer.sharedMaterial = newMaterial;

            //add MyAnimatorController3rdPerson
            AvatarAniController avatarAniController = MyHelpNode.FindOrAddComponent<AvatarAniController>(SeudoAvatar.transform);



            CreateFBBIK();
        }


        [ContextMenu("Create Full Body Biped IK")]
        public void CreateFBBIK()
        {
            _createFBBIK(SeudoAvatar.transform);
        }

        static void _createFBBIK(Transform ActorNode)
        {
            FullBodyBipedIK fbbik = MyHelpNode.FindOrAddComponent<FullBodyBipedIK>(ActorNode);
            BipedReferences references = new BipedReferences();
            BipedReferences.AutoDetectReferences(ref references, ActorNode, new BipedReferences.AutoDetectParams(true, false));

            Transform rootNode = IKSolverFullBodyBiped.DetectRootNodeBone(references);
            fbbik.SetReferences(references, rootNode);
        }
    }
}