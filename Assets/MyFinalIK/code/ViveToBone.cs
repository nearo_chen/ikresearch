﻿using UnityEngine;
using System.Collections;
using MyIKSetting;

public class ViveToBone : MonoBehaviour
{
    public Transform ViveLController, ViveRController, ViveHMD;
    public float Vive2PalmOffset;

    public bool DrawDebug = true;

    Transform _viveLPalmDebug, _viveRPalmDebug, _viveLController, _viveRController;
    bool _initDebug = false;

    // Update is called once per frame
    public void Update()
    {
        if (!DrawDebug) return;
        Vector3 scale = Vector3.one * 0.01f;

        if (!_initDebug)
        {
            _initDebug = true;
            Transform debugParent = MyHelpNode.FindOrNew_InChild(transform, "Debug_ViveToBone");


            //Create controller position
            _viveLController = MyHelpNode.FindOrNew_InChild(debugParent, "Debug_viveLController");
            MyHelpNode.AddMesh(_viveLController.gameObject, PrimitiveType.Cube, TotalDefine.colorViveDevice);
            _viveLController.transform.localScale = scale;

            _viveRController = MyHelpNode.FindOrNew_InChild(debugParent, "Debug_viveRController");
            MyHelpNode.AddMesh(_viveRController.gameObject, PrimitiveType.Cube, TotalDefine.colorViveDevice);
            _viveRController.transform.localScale = scale;

            //Create palm position
            _viveLPalmDebug = MyHelpNode.FindOrNew_InChild(debugParent, "Debug_viveLPalmDebug");
            MeshRenderer renderer =
                MyHelpNode.AddMesh(_viveLPalmDebug.gameObject, PrimitiveType.Sphere, MyHelpDraw.GetShader_DisableZ());
            MyHelpDraw.SetColor_DisableZ(renderer.material, TotalDefine.colorIKEffector);
            _viveLPalmDebug.transform.localScale = scale;

            _viveRPalmDebug = MyHelpNode.FindOrNew_InChild(debugParent, "Debug_viveRPalmDebug");
            renderer =
                MyHelpNode.AddMesh(_viveRPalmDebug.gameObject, PrimitiveType.Sphere, MyHelpDraw.GetShader_DisableZ());
            MyHelpDraw.SetColor_DisableZ(renderer.material, TotalDefine.colorIKEffector);
            _viveRPalmDebug.transform.localScale = scale;
        }

        //Update debug
        _viveLController.transform.position = ViveLController.position;
        _viveRController.transform.position = ViveRController.position;

        _viveLPalmDebug.transform.position = GetVivePalmPosition(TotalDefine.ActorSide.left);
        _viveRPalmDebug.transform.position = GetVivePalmPosition(TotalDefine.ActorSide.right);

        //draw line from controller to palm
        float width = scale.x * 0.5f;
        Color color = Color.green;
        MyHelpDraw.LineDraw(_viveLPalmDebug.transform, _viveLController.transform.position, width, width, color);
        MyHelpDraw.LineDraw(_viveRPalmDebug.transform, _viveRController.transform.position, width, width, color);
    }

    public Vector3 GetVivePalmPosition(TotalDefine.ActorSide side)
    {
        Transform controller = GetViveController(side);

        Vector3 offsetVec = -controller.forward * Vive2PalmOffset;
        return controller.position + offsetVec;
    }

    public Transform GetViveController(TotalDefine.ActorSide side)
    {
        return (side == TotalDefine.ActorSide.left) ? ViveLController : ViveRController;
    }

    public void GetViveHMDPose(out Vector3 position, out Quaternion rotation)
    {
        position = ViveHMD.position;
        rotation = ViveHMD.rotation;
    }
}
