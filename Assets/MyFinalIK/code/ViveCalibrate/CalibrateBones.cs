﻿using UnityEngine;
using System.Collections;
using MyIKSetting;
using System.Collections.Generic;

namespace ViveCalibrate
{
    //[RequireComponent(typeof(ViveToBone))]
    public class CalibrateBones : MonoBehaviour
    {
        //public Transform HMD;//ViveL, ViveR, 
        ViveToBone _viveToBone;

        bool _LForwardDone, _LSideDone, _RForwardDone, _RSideDone;

        Material _LViveMaterial, _RViveMaterial;

        const string name_caliLStatus = "calibrate_LeftStatus";
        const string name_caliLShoulder = "calibrate_LeftShoulder";

        Transform _showViveL, _showViveR;//, _caliHMD;
        readonly Color _cali_InitColor = Color.red;
        readonly Color _cali_forwardColor = Color.blue;
        readonly Color _cali_doneColor = Color.green;

        public bool DrawDebug = true;
        Transform _debugLeftUArm, _debugRightUArm;
        List<Transform> _debugLeftRec = new List<Transform>();
        List<Transform> _debugRightRec = new List<Transform>();

        public void SetVive2Bone(ViveToBone v2b)
        {
            _viveToBone = v2b;
        }

        // Use this for initialization
        void Start()
        {
            //_viveToBone = GetComponent<ViveToBone>();
            _initData(TotalDefine.ActorSide.left);
            _initData(TotalDefine.ActorSide.right);
        }

        // Update is called once per frame
        void Update()
        {
            if (DrawDebug)
            {
                float w = 0.005f;
                if (_debugLeftUArm != null)
                {
                    MyHelpDraw.LineDraw(_debugLeftRec[0], _debugLeftUArm.position, w, w, Color.red);
                    MyHelpDraw.LineDraw(_debugLeftRec[1], _debugLeftUArm.position, w, w, Color.red);
                }


                if (_debugRightUArm != null)
                {
                    MyHelpDraw.LineDraw(_debugRightRec[0], _debugRightUArm.position, w, w, Color.red);
                    MyHelpDraw.LineDraw(_debugRightRec[1], _debugRightUArm.position, w, w, Color.red);
                }

            }
        }

        void _initData(TotalDefine.ActorSide side)//Transform viveL, Transform viveR, Transform HMD)
        {
            Transform viveController = _viveToBone.GetViveController(side);
            Transform calibrateVive = MyHelpNode.FindOrNew_InChild(viveController, MyIKHelp.getSideName(side, name_caliLStatus));
            if (side == TotalDefine.ActorSide.left)
                _showViveL = calibrateVive;
            else
                _showViveR = calibrateVive;

            MyHelpNode.AddMesh(calibrateVive.gameObject, PrimitiveType.Cylinder, _cali_InitColor);
            calibrateVive.localScale = Vector3.one * 0.01f;
            calibrateVive.localPosition = Vector3.forward * 0.07f;
        }

        public void TriggerCalibrate(TotalDefine.ActorSide side)
        {
            if (!_calibrateArm_Forward(side))
            {
                _calibrateArm_Side(side);
            }
        }

        void _setForwardDone(TotalDefine.ActorSide side, bool done)
        {
            if (side == TotalDefine.ActorSide.left)
                _LForwardDone = done;
            else
                _RForwardDone = done;
        }
        void _setSideDone(TotalDefine.ActorSide side, bool done)
        {
            if (side == TotalDefine.ActorSide.left)
                _LSideDone = done;
            else
                _RSideDone = done;
        }
        bool _isForwardDone(TotalDefine.ActorSide side)
        {
            return (side == TotalDefine.ActorSide.left) ? _LForwardDone : _RForwardDone;
        }
        bool _isSideDone(TotalDefine.ActorSide side)
        {
            return (side == TotalDefine.ActorSide.left) ? _LSideDone : _RSideDone;
        }
        MeshRenderer _getCalibrateShowRenderer(TotalDefine.ActorSide side)
        {
            return (side == TotalDefine.ActorSide.left)
                ? _showViveL.GetComponent<MeshRenderer>()
                : _showViveR.GetComponent<MeshRenderer>();
        }


        //list 0: is shoulder forward calibrate local position of HMD
        //list 1: is shoulder side calibrate local position of HMD
        List<Vector3> _caliShoulder_Left = new List<Vector3>();
        List<Vector3> _caliShoulder_Right = new List<Vector3>();

        //needs to first adjust
        bool _calibrateArm_Forward(TotalDefine.ActorSide side)
        {
            if (_isForwardDone(side))
                return false;
            Transform HMD = _viveToBone.ViveHMD;
            Renderer caliRenderer = _getCalibrateShowRenderer(side);
            Vector3 palmPos = _viveToBone.GetVivePalmPosition(side);//(side == TotalDefine.ActorSide.left) ? ViveL : ViveR);

            Plane p = new Plane(HMD.forward, HMD.position);
            float d = p.GetDistanceToPoint(palmPos);
            Vector3 s = palmPos + -p.normal * d;

            List<Vector3> caliShoulder = (side == TotalDefine.ActorSide.left) ? _caliShoulder_Left : _caliShoulder_Right;
            caliShoulder.Add(HMD.InverseTransformPoint(s));//shoulder position forward

            MyHelpDraw.SetColor_Standard(caliRenderer.material, _cali_forwardColor);

            Debug.Log("[CalibrateBones] " + side.ToString() + " arm forward_calibrate done");

            _setForwardDone(side, true);

            //Add debug position
            Transform _debugForward = MyHelpNode.FindOrNew_InChild(this.transform, MyIKHelp.getSideName(side, name_caliLShoulder) + "_forwrad");
            MyHelpNode.AddMesh(_debugForward.gameObject, PrimitiveType.Sphere, TotalDefine.colorViveDevice);
            _debugForward.localScale = Vector3.one * 0.01f;
            _debugForward.localPosition = transform.InverseTransformPoint(palmPos);
            List<Transform> debugRec = (side == TotalDefine.ActorSide.left) ? _debugLeftRec : _debugRightRec;
            debugRec.Add(_debugForward);

            return true;
        }

        //needs to second adjust
        void _calibrateArm_Side(TotalDefine.ActorSide side)
        {
            if (!_isForwardDone(side) || _isSideDone(side))
                return;

            Transform HMD = _viveToBone.ViveHMD;
            Vector3 palmPos = _viveToBone.GetVivePalmPosition(side);

            Plane p = new Plane(HMD.right * (float)side, HMD.position);
            float d = p.GetDistanceToPoint(palmPos);
            Vector3 s = palmPos + -p.normal * d;

            List<Vector3> caliShoulder = (side == TotalDefine.ActorSide.left) ? _caliShoulder_Left : _caliShoulder_Right;
            caliShoulder.Add(HMD.InverseTransformPoint(s));//shoulder position right

            Debug.Log("[CalibrateBones] " + side.ToString() + " arm side_calibrate done");
            _setSideDone(side, true);

            //Add debug position
            Transform _debugSide = MyHelpNode.FindOrNew_InChild(this.transform, MyIKHelp.getSideName(side, name_caliLShoulder) + "_side");
            MyHelpNode.AddMesh(_debugSide.gameObject, PrimitiveType.Sphere, TotalDefine.colorViveDevice);
            _debugSide.localScale = Vector3.one * 0.01f;
            _debugSide.localPosition = transform.InverseTransformPoint(palmPos);
            List<Transform> debugRec = (side == TotalDefine.ActorSide.left) ? _debugLeftRec : _debugRightRec;
            debugRec.Add(_debugSide);
        }

        public class CalibrateData
        {
            public float viveHMDMaxHeight;
            public float viveNeck2UArmLength;
            public float viveUArm2PalmLength;

            //public Vector3[] _debugRecWorld = new Vector3[2];
            //public Vector3? _debugShoulderWorld;
        }

        public CalibrateData CalibrateArm_Update(TotalDefine.ActorSide side)
        //out float viveHeadMaxHeight, out float viveNeck2ShoulderLength, out float viveShoulder2PalmLength, out Vector3 shoulderPositionLocal)
        {
            Transform HMD = _viveToBone.ViveHMD;

            List<Vector3> caliShoulder = (side == TotalDefine.ActorSide.left) ? _caliShoulder_Left : _caliShoulder_Right;
            Vector3 palmPos = _viveToBone.GetVivePalmPosition(side);

            if (_isForwardDone(side) && _isSideDone(side) && caliShoulder.Count != 0)
            {
                Vector3 forwardShoulderLocal = caliShoulder[0];//can trust up, right
                Vector3 sideShoulderLocal = caliShoulder[1];//can trust up, forward
                Vector3 UArmPositionWorld = HMD.TransformPoint(
                    new Vector3(forwardShoulderLocal.x, (forwardShoulderLocal.y + sideShoulderLocal.y) * 0.5f, sideShoulderLocal.z)
                    );

                Vector3 neckPositionWorld = HMD.TransformPoint(sideShoulderLocal);

                CalibrateData calibrateData = new CalibrateData();
                calibrateData.viveHMDMaxHeight = HMD.localPosition.y;
                calibrateData.viveNeck2UArmLength = (neckPositionWorld - UArmPositionWorld).magnitude;
                calibrateData.viveUArm2PalmLength = (UArmPositionWorld - palmPos).magnitude;

                Vector3 shoulderPositionLocal = this.transform.InverseTransformPoint(UArmPositionWorld);

                //Create a debug shoulder
                Transform _debugShoulder = MyHelpNode.FindOrNew_InChild(this.transform, MyIKHelp.getSideName(side, name_caliLShoulder));
                MyHelpNode.AddMesh(_debugShoulder.gameObject, PrimitiveType.Sphere, TotalDefine.colorJoint);
                _debugShoulder.localScale = Vector3.one * 0.01f;
                _debugShoulder.localPosition = shoulderPositionLocal;

                if (side == TotalDefine.ActorSide.left)
                {
                    _debugLeftUArm = _debugShoulder;
                    //calibrateData._debugRecWorld[0] = _debugLeftRec[0].transform.position;
                    //calibrateData._debugRecWorld[0] = _debugLeftRec[1].transform.position;
                }
                else
                {
                    _debugRightUArm = _debugShoulder;
                    //calibrateData._debugRecWorld[0] = _debugRightRec[0].transform.position;
                    //calibrateData._debugRecWorld[0] = _debugRightRec[1].transform.position;
                }
                //calibrateData._debugShoulderWorld = _debugShoulder.transform.position;

                //Change material
                MeshRenderer caliRenderer = _getCalibrateShowRenderer(side);
                MyHelpDraw.SetColor_Standard(caliRenderer.material, _cali_doneColor);

                caliShoulder.Clear();
                return calibrateData;
            }
            return null;
        }

        public void CalibrateArm_Reset(TotalDefine.ActorSide side)
        {
            _setForwardDone(side, false);
            _setSideDone(side, false);

            MeshRenderer caliRenderer = _getCalibrateShowRenderer(side);
            MyHelpDraw.SetColor_Standard(caliRenderer.material, _cali_InitColor);

            //Reset debug data
            if (side == TotalDefine.ActorSide.left)
            {
                foreach (Transform obj in _debugLeftRec)
                    Destroy(obj.gameObject);
                _debugLeftRec.Clear();
                _debugLeftUArm = null;
            }
            else
            {
                foreach (Transform obj in _debugRightRec)
                    Destroy(obj.gameObject);
                _debugRightRec.Clear();
                _debugRightUArm = null;
            }


        }
    }

}