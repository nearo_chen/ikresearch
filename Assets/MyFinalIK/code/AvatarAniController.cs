﻿using UnityEngine;
using System.Collections;
using RootMotion.FinalIK;


/// <summary>
/// Basic Mecanim Animator controller for 3rd person view.
/// </summary>
[RequireComponent(typeof(Animator))]
public class AvatarAniController : MonoBehaviour
{
    //public float blendSpeed = 4f; // Animation blending speed
    public float maxAngle = 80f; // Max angular offset from camera direction
                                 //public float moveSpeed = 1.5f; // The speed of moving the character with no root motion
                                 //public float rootMotionWeight; // Crossfading between procedural movement and root motion

    protected Animator animator; // The Animator

    protected virtual void Start()
    {
        animator = GetComponent<Animator>();
    }

    //// Moving the character
    //void OnAnimatorMove()
    //{
    //    velocity = Vector3.Lerp(velocity, transform.rotation * Vector3.ClampMagnitude(moveInput, 1f) * moveSpeed, Time.deltaTime * blendSpeed);

    //    // Crossfading between procedural movement and root motion.
    //    transform.position += Vector3.Lerp(velocity * Time.deltaTime, animator.deltaPosition, rootMotionWeight);
    //}

    float? _turnBodyTimeCount, _turnWay;
    const float _turnBodyTimeCountThreshold = 3;
    float _turnRadiansCount;

    public void Trun(Vector3 faceDirection)
    {
        // Get the facing direction relative to the character rotation
        Vector3 faceDirectionLocal = transform.InverseTransformDirection(faceDirection);

        // Get the angle between the facing direction and character forward
        float angle = Mathf.Atan2(faceDirectionLocal.x, faceDirectionLocal.z) * Mathf.Rad2Deg;

        if (Mathf.Abs(angle) > maxAngle)
        {
            float rotation = 0;
            if (angle > maxAngle)
                rotation = angle - maxAngle;
            else if (angle < -maxAngle)
                rotation = angle + maxAngle;
            transform.Rotate(Vector3.up, rotation);
        }
        else if (Mathf.Abs(angle) > maxAngle * 0.3f)
        {
            if (_turnBodyTimeCount == null)
                _turnBodyTimeCount = _turnBodyTimeCountThreshold;
            else
                _turnBodyTimeCount -= Time.deltaTime;
        }
        else
        {
            _turnBodyTimeCount = null;
        }

        if (_turnBodyTimeCount.HasValue && _turnBodyTimeCount.Value < 0)
        {
            _turnBodyTimeCount = null;
            _turnWay = 1.0f;
            if (angle < 0)
                _turnWay = -1.0f;
            _turnRadiansCount = Mathf.Abs(angle) * Mathf.Deg2Rad;
        }

        if (_turnWay.HasValue)
        {
            const float turnSpeedRad = Mathf.PI * 0.2f;
            float way = _turnWay.Value;
            float angleRadians = turnSpeedRad * Time.deltaTime;
            if (_turnRadiansCount - angleRadians <= 0)
            {
                _turnWay = null;
                angleRadians = _turnRadiansCount;
            }
            else
                _turnRadiansCount -= angleRadians;

            transform.localRotation *= Quaternion.AngleAxis(angleRadians * way * Mathf.Rad2Deg, Vector3.up);
        }
    }

    Vector3? _moveTarget;
    Vector3 _movingDir;
    Vector3 _moveBlend;

    // Move the character
    public void Move(Transform _debugPelvisIsolation, Transform _debugPelvis, float _movingThreshold)
    {
        Transform pseudoPelvis = animator.GetBoneTransform(HumanBodyBones.Hips);
        Vector3 moveDirWorld = pseudoPelvis.position - transform.position;//because animator.transform has scale, so don't use the animator.transform.Inverse.....
        moveDirWorld.y = 0;
        float movingMagnitude = moveDirWorld.magnitude;
        moveDirWorld = moveDirWorld / movingMagnitude;
        Vector3 moveDirLocal = transform.InverseTransformDirection(moveDirWorld);

        if (_debugPelvisIsolation)
            MyHelpDraw.LineDraw(_debugPelvisIsolation, _debugPelvisIsolation.position + transform.forward * _movingThreshold, 0.02f, 0.002f, Color.green);
        if (_debugPelvis)
            MyHelpDraw.LineDraw(_debugPelvis, _debugPelvis.position + moveDirWorld * movingMagnitude, 0.02f, 0.002f, Color.yellow);

        if (movingMagnitude > _movingThreshold)
        {
            //_moveBlend = Vector3.Lerp(_moveBlend, moveDirLocal, Time.deltaTime * blendSpeed);
            _moveBlend = moveDirLocal;

            // Set Animator parameters
            animator.SetFloat("X", _moveBlend.x);
            animator.SetFloat("Z", _moveBlend.z);
            animator.SetBool("IsMoving", true);

            float controlPlaySpeed = movingMagnitude / _movingThreshold;
            animator.speed = Mathf.Clamp(controlPlaySpeed, 1, controlPlaySpeed);

            _moveTarget = pseudoPelvis.position;
            _movingDir = moveDirLocal;
        }
        else
        {
            if (_moveTarget.HasValue &&
                Vector3.Dot(_movingDir, transform.InverseTransformPoint(_moveTarget.Value)) <= 0
              )
            {
                transform.position = new Vector3(_moveTarget.Value.x, transform.position.y, _moveTarget.Value.z);
                _moveTarget = null;
                animator.SetBool("IsMoving", false);
                _moveBlend = Vector3.zero;
                
            }
        }


    }
}

