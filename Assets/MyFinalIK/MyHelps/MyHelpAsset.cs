﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;

public static class MyHelpAsset
{
    public static T TryLoadAsset<T>(string assetFullPathName) where T : UnityEngine.Object
    {   
        T asset = AssetDatabase.LoadAssetAtPath<T>(assetFullPathName);
        if (asset == null)
            Debug.Log("[TryLoadAsset] not found : " + assetFullPathName);
        return asset;
    }

    public static void TryToCreateFolder(string folderParentPath, string folderName)
    {
        string full = folderParentPath + "/" + folderName;
        if (!AssetDatabase.IsValidFolder(full))
            AssetDatabase.CreateFolder(folderParentPath, folderName);
    }

    public static T TryToCreateAsset<T>(string folderParentPath, string folderName, string assetFileName, T assetData) where T : UnityEngine.Object
    {
        string fullName = folderParentPath + "/" + folderName + "/" + assetFileName;
        T loadAsset = TryLoadAsset<T>(fullName);
        if (loadAsset == null)
        {
            TryToCreateFolder(folderParentPath, folderName);            
            AssetDatabase.CreateAsset(assetData, fullName);
          //  AssetDatabase.SaveAssets();
          //  AssetDatabase.Refresh();
            loadAsset = assetData;

            Debug.Log("[MyHelpAsset] TryToCreateAsset : " + fullName);
        }

        return loadAsset;
    }
}
#endif
