﻿using UnityEngine;
using System.Collections;
using Valve.VR;

public class ViveHelp : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public static bool IsTracking(SteamVR_TrackedObject obj)
    {
        return IsTracking(obj, false);
    }

    public static bool IsTracking(SteamVR_TrackedObject obj, bool forceTracking)
    {
        if (forceTracking)
            return true;

        if (obj == null)
            return false;

        if (!obj.isValid)
        {
            return false;
        }

        var device = SteamVR_Controller.Input((int)obj.index);
        if (device == null)
        {
            return false;
        }

        return device.hasTracking;
    }

    public enum PressState
    {
        Down,
        Up,
    }
    public static bool IsTriggerPress(SteamVR_TrackedObject obj, PressState state)
    {
        if (obj == null)
            return false;

        if(state == PressState.Down)
            return IsTracking(obj) && SteamVR_Controller.Input((int)obj.index).GetPressDown(EVRButtonId.k_EButton_SteamVR_Trigger);
        else if(state == PressState.Up)
            return IsTracking(obj) && SteamVR_Controller.Input((int)obj.index).GetPressUp(EVRButtonId.k_EButton_SteamVR_Trigger);
        return false;
    }

    public static bool IsTouchpadPress(SteamVR_TrackedObject obj, PressState state)
    {
        if (obj == null)
            return false;

        if (state == PressState.Down)
            return IsTracking(obj) && SteamVR_Controller.Input((int)obj.index).GetPressDown(EVRButtonId.k_EButton_SteamVR_Touchpad);
        else if (state == PressState.Up)
            return IsTracking(obj) && SteamVR_Controller.Input((int)obj.index).GetPressUp(EVRButtonId.k_EButton_SteamVR_Touchpad);
        return false;
    }
}
