﻿using UnityEngine;
using System.Collections;

public class RagDollSetting : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    [ContextMenu("Close all use gravity")]
    void CloseAllUseGravity()
    {
        _setAllUseGravity(transform, false);
    }

    [ContextMenu("Open all use gravity")]
    void OpenAllUseGravity()
    {
        _setAllUseGravity(transform, true);
    }

    static void _setAllUseGravity(Transform parent, bool value)
    {   
        Rigidbody rb = parent.GetComponent<Rigidbody>();
        if (rb != null)
            rb.useGravity = value;

        for (int a = 0; a < parent.childCount; a++)
            _setAllUseGravity(parent.GetChild(a), value);
    }

    [ContextMenu("Set all isKinematic")]
    void SetIsKinematic()
    {
        _setIsKinematic(transform, true);
    }

    [ContextMenu("Set all notKinematic")]
    void SetNotKinematic()
    {
        _setIsKinematic(transform, false);
    }

    static void _setIsKinematic(Transform parent, bool value)
    {
        Rigidbody rb = parent.GetComponent<Rigidbody>();
        if (rb != null)
            rb.isKinematic = value;

        for (int a = 0; a < parent.childCount; a++)
            _setIsKinematic(parent.GetChild(a), value);
    }
}
