﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MyHelpNode
{
    public static void FindTransform(Transform root, string findName, ref Transform outFind)
    {
        if (outFind != null)
            return;

        //Debug.Log("find : " + root.name);
        if (root.name == findName)
        {
            //Debug.Log("find success xxxxxxxxxxxxxxxxxxxxxxxxx: " + root.name);
            outFind = root;
            return;
        }

        for (int a = 0; a < root.childCount; a++)
            FindTransform(root.GetChild(a), findName, ref outFind);
    }

    public static MeshRenderer AddMesh(GameObject obj, PrimitiveType type, Shader shader)
    {
        return AddMesh(obj, type, Color.white, shader);
    }

    public static MeshRenderer AddMesh(GameObject obj, PrimitiveType type, Color color)
    {
        return AddMesh(obj, type, color, null);
    }

    public static MeshRenderer AddMesh(GameObject obj, PrimitiveType type, Color color, Shader shader)
    {
        //http://answers.unity3d.com/questions/635991/how-to-assign-icosphere-to-meshfilter-with-script.html
        GameObject temp = GameObject.CreatePrimitive(type);
        temp.SetActive(false);

        Material diffuse = null;
        if (shader != null)
        {
            diffuse = new Material(shader);
        }
        else
        {
            //diffuse = MonoBehaviour.Instantiate(temp.GetComponent<MeshRenderer>().sharedMaterial) as Material;
            diffuse = new Material(Shader.Find("Standard"));
            diffuse.SetColor("_Color", color);
        }

        //Save material to asset


#if UNITY_EDITOR
        string assetFileName =
            diffuse.shader.name.Replace('/', '.')
            + color.ToString().Replace("RGBA", "") + ".mat";

        diffuse = MyHelpAsset.TryToCreateAsset<Material>("Assets/Resources", "MyHelpNode", assetFileName, diffuse);
#else
        string assetFileName =
            diffuse.shader.name.Replace('/', '.')
            + color.ToString().Replace("RGBA", "");

        diffuse = Resources.Load<Material>("MyHelpNode/"+ assetFileName);
        if(diffuse == null)
            Debug.LogError("[Not found] ---> MyHelpNode/"+ assetFileName);
#endif

        MeshFilter meshFilter = obj.GetComponent<MeshFilter>();
        if (meshFilter == null)
            meshFilter = obj.AddComponent<MeshFilter>();
        Mesh mesh = temp.GetComponent<MeshFilter>().mesh;// MonoBehaviour.Instantiate(temp.GetComponent<MeshFilter>().mesh) as Mesh;      (must not use clone,or,it'll not save in prefeb)  

        //Save mesh to asset(not work)
        //string meshAssetFileName = type.ToString() + "_mesh";
        //mesh = MyHelpAsset.TryToCreateAsset<Mesh>("Assets/Resources", "MyHelpNode", meshAssetFileName, mesh);

        meshFilter.mesh = mesh;
        MonoBehaviour.DestroyImmediate(temp);

        //set renderer
        MeshRenderer renderer = obj.GetComponent<MeshRenderer>();
        if (renderer == null)
            renderer = obj.AddComponent<MeshRenderer>();
        renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        renderer.receiveShadows = false;

        renderer.material = diffuse;

        return renderer;
    }

    /// <summary>
    /// Notice: this function will reset position rotation!
    /// </summary>
    public static Transform FindOrNew_InChild(Transform parent, string name)
    {
        Transform find = null;
        FindTransform(parent, name, ref find);

        if (find == null)
        {
            GameObject obj = new GameObject();
            obj.name = name;
            find = obj.transform;
        }
        find.transform.parent = parent;
        find.transform.localPosition = Vector3.zero;
        find.transform.localRotation = Quaternion.identity;
        find.transform.localScale = Vector3.one;
        return find;
    }


    public static T FindOrAddComponent<T>(Transform node) where T : Component
    {
        T component = node.gameObject.GetComponent<T>();
        if (component == null)
            component = node.gameObject.AddComponent<T>();
        return component;
    }


    public static float GetWorldLength(List<Transform> nodeList)
    {
        float length = 0;
        Vector3 oldPos = nodeList[0].position;
        for (int a = 1; a < nodeList.Count; a++)
        {
            Vector3 currPos = nodeList[a].position;
            length += (oldPos - currPos).magnitude;
            oldPos = currPos;
        }
        return length;
    }
}