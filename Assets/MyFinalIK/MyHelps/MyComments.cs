﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Adding comments to GameObjects in the Inspector.
/// </summary>
public class MyComments : MonoBehaviour
{
    /// <summary>
    /// The comment.
    /// </summary>
    [Multiline]
    public string text;
}